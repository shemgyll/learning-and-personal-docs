## Learning and Personal docs
This project is for my own learning and documentation/cheat sheets

### Note
Documentation and notes here are available in the handbook or docs, mirrored here freehand for my own reference.

Anything here should always be added to the handbook/docs if not there already.

### Commands
Legacy to hashed repos error check
`cat /var/log/gitlab/sidekiq/current | grep <project-name, etc> | jq . ` 

### Repositories
[GitLab + LDAP seeded docker-compose files](https://gitlab.com/gitlab-com/support/toolbox/replication/-/tree/master/compose_files)
